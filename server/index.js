// index.js
const express = require("express");
const fileUpload = require('express-fileupload');
const moment = require('moment')
fs = require('fs');

require('dotenv').config()

port = 3000;

allowed_extensions = ['gif', 'jpg', 'png', 'jpeg', 'bmp']
FILE_DIRECTORY = process.env.FILE_DIRECTORY || __dirname + '/../files/'

const app = express();
// Specify location of views
app.set("views", "./server/views");
app.use(fileUpload());


app.get("/", (req, res) => {res.send("hi, i am a robot")})
app.post('/text', async (req, res) => {
  console.log((new Date()).toLocaleString(), req.body.text);

  await printText(req.body.text);

  res.send("thanks for the message! " + (new Date()).toLocaleString())

})
app.post("/upload", (req, res) => {
  let image;
  let uploadPath;

  console.log(req, req.files);

  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
  }

  // The name of the input field (i.e. "image") is used to retrieve the uploaded file
  image = req.files.image;
  extension = image.name.split('.').pop().toLocaleLowerCase()
  if (!allowed_extensions.includes(extension)){
    return res.status(400).send('File must be gif, jpg, png, jpeg, bmp');
  }

  uploadPath = FILE_DIRECTORY + (new Date()).toISOString() + extension;
  console.log(`saving file ${uploadPath}`);
  // Use the mv() method to place the file somewhere on your server
  image.mv(uploadPath, function(err) {
    if (err)
      return res.status(500).send(err);

    // subprocess.run(["lpr", "-o orientation-requested=" + ("3" if h/(1.0*w) > 1.5 else "4"), "-o fit-to-page -o media=Custom.72x1000mm", "-P", "mibble", att_path])


    res.send('Printing!');
  });
});

app.listen(port, () => console.log(`Server running on http://localhost:3000`));


async function printText(text) {
  let output = [(new Date()).toLocaleString(), "", text]
  output = output.concat(Array(8).fill(''))
  fs.writeFile('/dev/usb/lp1', output.join('\r\n'), (err) => console.log(err))
}

async function printLine(line) {
  console.log("printing", line);
  await fs.writeFile('/dev/usb/lp1', line+"\r\n", (err) => console.log(err))
}

function prettyTime(date) {
  diff = moment().diff(moment(date))
  duration = moment.duration(diff)

  secs = diff / 1000

  if (secs < 60) { return `${duration.seconds()} seconds`}
  if (secs < 60 * 60) { return `${duration.minutes()} minutes`}
  if (secs < 60 * 60 * 24) { return `${duration.hours()} hours`}
  return `${duration.days()} days`
}
