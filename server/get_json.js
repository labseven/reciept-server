const http = require('http');


// const options_get = {
//   host: ha_config.host,
//   port: ha_config.port,
//   path: '/api/states/switch.coffee_machine',
//   method: 'GET',
//   headers: {
//     'Content-Type': 'application/json',
//     'Authorization': `Bearer ${ha_config.auth_header}`
//   }
// };

// const options_post = {
//   host: ha_config.host,
//   port: ha_config.port,
//   path: '/api/services/script/blink_office_lamp',
//   method: 'POST',
//   headers: {
//     'Content-Type': 'application/json',
//     'Authorization': `Bearer ${ha_config.auth_header}`
//   }
// };

/**
 * getJSON:  RESTful GET request returning JSON object(s)
 * @param options: http options object
 * @param callback: callback to pass the results JSON object(s) back
 */

module.exports.getJSON = (options, onResult) => {
    console.log('rest::getJSON');
    const port = options.port == 443 ? https : http;

    let output = '';

    const req = port.request(options, (res) => {
        console.log(`${options.host} : ${res.statusCode}`);
        res.setEncoding('utf8');

        res.on('data', (chunk) => {
            output += chunk;
        });

        res.on('end', () => {
            let obj = JSON.parse(output);

            onResult(res.statusCode, obj);
            console.log(obj);
        });
    });

    req.on('error', (err) => {
        // res.send('error: ' + err.message);
        console.log(err);
    });

    req.end();
};

module.exports.postJSON = (options, onResult) => {
    console.log('rest::postJSON');
    const port = options.port == 443 ? https : http;
    let output = '';

    const req = port.request(options, (res) => {
        console.log(`${options.host} : ${res.statusCode}`);
        res.setEncoding('utf8');

        res.on('data', (chunk) => {
            output += chunk;
        });

        res.on('end', () => {
            try {
                obj = JSON.parse(output);
            } catch(e){
                obj = output
            }

            onResult(res.statusCode, obj);
            console.log(obj);
        });
    });

    req.on('error', (err) => {
        // res.send('error: ' + err.message);
        console.log(err);
    });

    req.end();
};
